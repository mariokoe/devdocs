## Local build

To look at your changes locally you can do the following.

* If you haven't already done so, run `npm install` to install the dependencies.
* Run `npm run serve` to generate documentation and start local server for preview.
  * You can see the result at `http://localhost:4000`.
  * As long as server running, you can make changes to the documentation and the pages will be updated automatically.
  * `Ctrl+C` in console will shut down the server.

## GitBook specific

If you are new to GitBook please consider this in order to avoid issues:

* When adding new page, do not forget to add it to [book/SUMMARY.md](book/SUMMARY.md).
* If you going to use some new macro, do not forget to add plugin to [package.json](package.json) and [book.json](book.json). Re-run `npm install` after adding new dependencies.
