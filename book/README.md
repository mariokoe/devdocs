---
description: >-
  On this site, you'll find developer documentation that you can use to work
  with and integrate eyeo's ad-filtering SDKs.
---

# Getting Started

### About eyeo

[eyeo](https://eyeo.com/) is dedicated to building technology that supports a fair and sustainable online value exchange between users, publishers and advertisers.&#x20;

eyeo Ad-Filtering Solutions offer customizable options to enhance your mobile and desktop products with minimal technical effort.&#x20;

### Ad-filtering SDKs

eyeo offers several ad-filtering SDKs for your development projects across different platforms and browsers. Jump right in with the [eyeo Chromium SDK documentation](sdks/eyeo-chromium-sdk/quickstart.md#about-the-eyeo-chromium-sdk).

If you need help with another eyeo SDK or you're interested in becoming a tech partner, [reach out to the eyeo team](https://eyeo.com/solutions/tech-partners#paragraph-63).
