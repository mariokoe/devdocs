# Adblock API documentation

This document provides guidance (including code snippets) on using both [Java](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/blob/eyeo-109-dev/components/adblock/android/java/src/org/chromium/components/adblock/AdblockController.java) and [Javascript](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/blob/eyeo-109-dev/components/adblock/android/java/src/org/chromium/chrome/browser/extensions/api/adblock_private/adblock_private_api.h) ad-filtering APIs in your browser.

## Toggle ad-blocking

To toggle ad-blocking use `setEnabled` with the appropriate `bool` value:

To query if ad-blocking is enabled use `isEnabled`.

The following example disables adblocking:

{% tabs %}
{% tab title="Java" %}
```java
import org.chromium.components.adblock.AdblockController;

AdblockController.getInstance().isEnabled();  // true
AdblockController.getInstance().setEnabled(false);
AdblockController.getInstance().isEnabled();  // false
```
{% endtab %}
{% tab title="JavaScript" %}
Since version 92:
```javascript
chrome.adblockPrivate.isEnabled();  // true
chrome.adblockPrivate.setEnabled(false);
chrome.adblockPrivate.isEnabled();  // false
```
{% endtab %}
{% endtabs %}

## Toggle Acceptable Ads

Acceptable ads can be toggled in a similar way with the `setAcceptableAdsEnabled` API call.

To query if Acceptable Ads are enabled use `isAcceptableAdsEnabled`.

The following example disabled Acceptable Ads:

{% tabs %}
{% tab title="Java" %}
```java
import org.chromium.components.adblock.AdblockController;

AdblockController.getInstance().isAcceptableAdsEnabled();  // true
AdblockController.getInstance().setAcceptableAdsEnabled(false);
AdblockController.getInstance().isAcceptableAdsEnabled();  // false
```
{% endtab %}
{% tab title="JavaScript" %}
Since version 92:
```javascript
chrome.adblockPrivate.isAcceptableAdsEnabled();  // true
chrome.adblockPrivate.setAcceptableAdsEnabled(false);
chrome.adblockPrivate.isAcceptableAdsEnabled();  // false
```
{% endtab %}
{% endtabs %}

Note: Acceptable Ads can be enabled/disabled with `installSubscription`/`uninstallSubscription` methods too.


## Add/Remove subscriptions

Use `installSubscription` to add and `uninstallSubscription` to remove a filter list.

To get the list of installed subscriptions use `getInstalledSubscriptions`.

The following code snippet installs `example_list`:

{% tabs %}
{% tab title="Java" %}
```java
import org.chromium.components.adblock.AdblockController;

URL exampleFilterList = new URL("http://example.com/example_list.txt");
AdblockController.getInstance().installSubscription(exampleFilterList);
AdblockController.getInstance().getInstalledSubscriptions();  // ["http://example.com/example_list.txt", ...]
```
{% endtab %}
{% tab title="JavaScript" %}
Since version 92:
```javascript
var exampleFilterList = new URL("http://example.com/example_list.txt");
chrome.adblockPrivate.installSubscription(exampleFilterList.href);
chrome.adblockPrivate.getInstalledSubscriptions();  // ["http://example.com/example_list.txt", ...]
```
{% endtab %}
{% endtabs %}

To get a list of suggested subscriptions call `getRecommendedSubscriptions` method in Java and `getBuiltInSubscriptions` in JavaScript


## Enable/Disable ad-blocking on a specific domain

Use `addAllowedDomain` to stop filtering ads on a specific domain, and `removeAllowedDomain` to resume.

`getAllowedDomains` returns a list of allowed domains.

{% tabs %}
{% tab title="Java" %}
```java
import org.chromium.components.adblock.AdblockController;

AdblockController.getInstance().addAllowedDomain("example.com");
AdblockController.getInstance().getAllowedDomains();  // ["example.com"]
AdblockController.getInstance().removeAllowedDomain("example.com");
AdblockController.getInstance().getAllowedDomains();  // []
```
{% endtab %}
{% tab title="JavaScript" %}
Since version 92:
```javascript
chrome.adblockPrivate.addAllowedDomain("example.com");
chrome.adblockPrivate.getAllowedDomains();  // ["example.com"]
chrome.adblockPrivate.removeAllowedDomain("example.com");
chrome.adblockPrivate.getAllowedDomains();  // []
```
{% endtab %}
{% endtabs %}

Note: Pass a *domain* ('example.com') as an argument, not a URL ('http://www.example.com/page.html').


## Add/Remove custom filters

Use `addCustomFilter` to add and `removeCustomFilter` to remove a single filter.

To get the list of custom filters added use `getCustomFilters`.

The following code snippet installs a new filter:

{% tabs %}
{% tab title="Java" %}
```java
import org.chromium.components.adblock.AdblockController;

AdblockController.getInstance().addCustomFilter("example_domain##.example_selector");
AdblockController.getInstance().getCustomFilters();  // ["example_domain##.example_selector"]
AdblockController.getInstance().removeCustomFilter("example_domain##.example_selector");
AdblockController.getInstance().getCustomFilters();  // []
```
{% endtab %}
{% tab title="JavaScript" %}
Since version 92:
```javascript
chrome.adblockPrivate.addCustomFilter("example_domain##.example_selector");
chrome.adblockPrivate.getCustomFilters();  // ["example_domain##.example_selector"]
chrome.adblockPrivate.removeCustomFilter("example_domain##.example_selector");
chrome.adblockPrivate.getCustomFilters();  // []
```
{% endtab %}
{% endtabs %}
