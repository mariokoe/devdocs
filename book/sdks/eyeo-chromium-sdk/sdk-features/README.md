---
description: Learn how the eyeo Chromium SDK helps you filter content.
---

# SDK Features

### eyeo Chromium SDK Features

This page contains information you'll find useful as you configure the eyeo Chromium SDK for your own project.&#x20;

If you've not yet installed the SDK, start with the [eyeo Chromium SDK quickstart guide](../quickstart.md).

Check out the detailed [API documentation](api-documentation.md) to learn how to interact with eyeo Chromium SDK

{% hint style="info" %}
Some knowledge of Java and C++ will benefit you as you implement the practices on this page.
{% endhint %}

### Supported ad-filtering techniques

The eyeo Chromium SDK supports the following ad-filtering techniques:

* [Resource filtering](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/blob/eyeo-110-dev/components/adblock/docs/ad-filtering/resource-filtering.md), which blocks network resources from downloading
* [Element hiding](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/blob/eyeo-110-dev/components/adblock/docs/ad-filtering/element-hiding.md), which hides content that request-blocking couldn't hide
* [Snippets](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/blob/eyeo-110-dev/components/adblock/docs/ad-filtering/snippets.md); custom code snippets that deal with complex cases, like video ads

Because ad content can be served at various stages while a web page loads, the eyeo Chromium SDK consults filter rules to determine which requests to block and which to hide.

### How eyeo Chromium SDK uses filter lists

To determine which requests to block and which elements to hide, the eyeo Chromium SDK refers to filter lists, collections of rules that instruct a browser to block certain elements on a web page.

Filter lists are hosted online in plain text in [well-known format](https://help.eyeo.com/adblockplus/how-to-write-filters). eyeo Chromium SDK downloads these lists, converts them into the [FlatBuffer format](https://google.github.io/flatbuffers/), and stores them. The eyeo Chromium SDK implements all ad-filtering techniques, then, and deploys the right technique depending on the nature of the ad content it detects.

#### Default filter lists

A basic eyeo Chromium SDK installation contains the following filter lists:

* [**EasyList**](https://easylist.to/); the standard for filtering ad content online. eyeo Chromium SDK also includes language-specific rules that depend on the user's system region
* **Acceptable Ads**; an allowlist of ad content that the [Acceptable Ads Committee](https://acceptableads.com/) has compiled
* **ABP Filters (Anti-Circumvention)**; custom filter lists, including the ABP anti-circumvention filter list

{% hint style="success" %}
The Acceptable Ads and ABP Filters lists are both maintained by eyeo.
{% endhint %}

### Recipes for common ad-filtering integration tasks

Depending on your implementation of the eyeo Chromium SDK, you'll likely find some of the following strategies useful for configuring the SDK within your project.

{% hint style="info" %}
The following strategies are only supported for Android.
{% endhint %}

#### Set the application identifier

eyeo recommends overriding the values returned from `version_info::GetProductName()` and `version_info::GetVersionNumber()` by setting the values of `PRODUCT_NAME` and `PRODUCT_VERSION` constants:

* `PRODUCT_NAME` is derived from the `chrome/app/theme/chromium/BRANDING` file.
* `PRODUCT_VERSION` is derived from the `chrome/VERSION` file.

You can either modify these files or have the functions return different strings.

These changes could affect your application. If that impacts your use case, you can still override the values with the GN `eyeo_application_name` and `eyeo_application_version` variables:

{% code overflow="wrap" %}
```bash
gn gen --args='eyeo_application_name="My great browser" eyeo_application_version="99.99.0.1"...' ...
```
{% endcode %}

{% hint style="warning" %}
gn `abp_application_name` and `abp_application_version` variables are deprecated since eyeo Chromium SDK version 105.
{% endhint %}


#### Get notifications about blocked or allowed network requests

Implement the `AdblockController.AdBlockedObserver` interface.

Add your observer by calling `AdblockController.getInstance().addOnAdBlockedObserver()` and remove it by calling `AdblockController.getInstance().removeOnAdBlockedObserver()`. Don't add an `AdBlockedObserver` if you won't consume these notifications, as it has a small performance penalty.


#### Change default filter lists

By default, the eyeo Chromium SDK includes EasyList, Acceptable Ads, and the ABP Filters (Anti-Circumvention) filter list.

If you want to change the default filter lists in your eyeo Chromium project, modify `config::GetKnownSubscriptions` in `components/adblock/core/subscription_config.cc`.

The following shows a sample configuration for designating a new filter list as default:

```cpp
static std::vector<SubscriptionInfo> recommendations = {
    ...
     {"https://domain.com/subscription.txt",        // URL
      "My custom filters",                          // Display name for settings
      {},                                           // Supported languages list, considered for
                                                    // SubscriptionFirstRunBehavior::SubscribeIfLocaleMatch
      SubscriptionUiVisibility::Visible,            // Should the app show a subscription in the settings
      SubscriptionFirstRunBehavior::Subscribe       // Should the app subscribe on first run
      SubscriptionPrivilegedFilterStatus::Forbidden // Allow snippets and header filters or not
     },
```

#### Bundle filter lists

Bundling filter lists lets the browser use the lists without downloading them from a server. This is especially useful for slow or unreliable networks.

eyeo provides the following bundled filter lists:

* `components/resources/adblocking/anticv.txt`
* `components/resources/adblocking/easylist.txt`
* `components/resources/adblocking/exceptionrules.txt`

Follow these steps to bundle more filter lists:

1. Download the filter list and place it in `components/resources/adblocking`, for example, `components/resources/adblocking/my_list.txt`.
2.  Add a target that converts the text-format list into a FlatBuffer in `components/resources/adblocking/BUILD.gn`:

    ```
    make_preloaded_subscription("make_my_list") {
      input = "//components/resources/adblocking/my_list.txt"
      url = "https://my.server.com/my_list.txt"
      output = "${target_gen_dir}/my_list.fb"
    }

    group("make_all_preloaded_subscriptions") {
      deps = [
        ":make_anticv",
        ":make_easylist",
        ":make_exceptionrules",
        ":make_my_list",
      ]
    }
    ```
3.  Add the generated FlatBuffer file into the ResourceBundle with `components/resources/adblock_resources.grdp`:

    {% code overflow="wrap" %}
    ```
    <include name="IDR_ADBLOCK_FLATBUFFER_MY_LIST" file="${root_gen_dir}/components/resources/adblocking/my_list.fb" use_base_dir="false" type="BINDATA" compress="gzip" />
    ```
    {% endcode %}
4.  Instruct `PreloadedSubscriptionProviderImpl` at `components/adblock/core/subscription/preloaded_subscription_provider_impl.h` when to provide the bundled list:

    ```cpp
    class PreloadedSubscriptionProviderImpl final
        : public PreloadedSubscriptionProvider {

      ...
      scoped_refptr<Subscription> preloaded_anticv_;
      scoped_refptr<Subscription> preloaded_my_list_;
    };
    ```

    And in `components/adblock/core/preloaded_subscription_provider_impl.cc`

    ```cpp
    std::vector<scoped_refptr<Subscription>>
    PreloadedSubscriptionProviderImpl::GetCurrentPreloadedSubscriptions() const {
      std::vector<scoped_refptr<Subscription>> result;
      if (preloaded_easylist_)
        result.push_back(preloaded_easylist_);
      if (preloaded_exceptionrules_)
        result.push_back(preloaded_exceptionrules_);
      if (preloaded_anticv_)
        result.push_back(preloaded_anticv_);
      if (preloaded_my_list_)
        result.push_back(preloaded_my_list_);
      return result;
    }

    void PreloadedSubscriptionProviderImpl::UpdateSubscriptionsInternal() {
      UpdatePreloadedSubscription(preloaded_easylist_, "*easylist.txt",
                                  installed_subscriptions_, pending_subscriptions_,
                                  IDR_ADBLOCK_FLATBUFFER_EASYLIST);
      UpdatePreloadedSubscription(preloaded_exceptionrules_, "*exceptionrules.txt",
                                  installed_subscriptions_, pending_subscriptions_,
                                  IDR_ADBLOCK_FLATBUFFER_EXCEPTIONRULES);
      UpdatePreloadedSubscription(preloaded_anticv_, "*abp-filters-anti-cv.txt",
                                  installed_subscriptions_, pending_subscriptions_,
                                  IDR_ADBLOCK_FLATBUFFER_ANTICV);
      UpdatePreloadedSubscription(preloaded_my_list_, "*my_list.txt",
                                  installed_subscriptions_, pending_subscriptions_,
                                  IDR_ADBLOCK_FLATBUFFER_MY_LIST);
    }
    ```

These steps have added a version of `https://my.server.com/my_list.txt` into the ResourceBundle. This bundled filter list will be used in these circumstances:

* `https://my.server.com/my_list.txt` has been requested to install but hasn't finished downloading
* A website is loading and requires ad-filtering functions immediately

The bundled filter list gets replaced by a version of the filter list downloaded from the internet as soon as it becomes available.

#### How to test filters

eyeo provides testing pages for eyeo Chromium SDK's ad-blocking features at [ABP Test Pages](https://testpages.adblockplus.org/). Subscribe to the [test filter list](https://testpages.adblockplus.org/en/abp-testcase-subscription.txt) before loading the test pages.&#x20;

To learn how to subscribe to a specific filter list, read [how to add and remove filter lists.](broken-reference)

#### Find out what has changed between eyeo Chromium SDK releases

Differences across versions are listed in [the eyeo Chromium SDK changelog](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/blob/eyeo-103-dev/components/adblock/CHANGELOG.md).

You can also use our interdiff script to compare two git revision ranges.

The script requires the `patchutils` package to be available.

* On Mac, run `brew install patchutils` to install the package.
* On Linux, use your package manager, like `sudo apt install patchutils`.

To get a combined patch, run the script to get changes introduced by eyeo Chromium SDK compared to vanilla Chromium in the old branch, and repeat  for the new branch.&#x20;

Use the release announcements to get the correct hashes for the revision ranges.&#x20;

For help on how to use the script, run `./interdiff.py --help`.
