---
description: Integration testing for eyeo Chromium SDK.
---
# Testing integration of the eyeo Chromium SDK

## Settings UI (Android)

If you have chosen to use the default Settings UI, you will find it in the lower part of Chromium's Settings pane:


<img src="images/settings-main.jpg" alt="Screenshot: Chromium settings UI showing the Adblocking fragment" width="400"/>

This is how the default Settings UI looks like and behaves:

<img src="images/adblocking-menu.jpg" alt="Screenshot: adblocking settings UI" width="400"/>

1. **Adblock Plus** : Enable/Disable ad-blocking for the entire browser. Toggling this button off also disables all other menu options.
2. **Language Filters**: Select which filter lists are installed.
3. **Acceptable Ads**: Enable/Disable Acceptable Ads. When this switch is enabled, the browser will show Acceptable Ads.
4. **Allowlist**: Maintain a list of domains on which ad-blocking is disabled. No ads will be hidden or blocked on these domains.

### Developer options for testing

Advanced users, or QA/Testers, can unlock `More Blocking Options` by toggling the `Adblock Plus` switch 10 times.

<img src="images/developer-options-enabled.jpg" alt="Screenshot: developer options enabled in the UI" width="400"/>

In this menu, you can add or remove:
- custom filter lists, like `https://abptestpages.org/en/abp-testcase-subscription.txt`
- custom filters, like `||abptestpages.org/testfiles/document/*`

<img src="images/more-blocking-options.jpg" alt="Screenshot: content of developer options view" width="400"/>

**Warning:** Custom filters and custom filter lists can break website rendering, do not encourage regular users to change these settings.

## Smoke testing

You may use these scenarios for quickly checking if ad-filtering works in your browser.

**Scenario 1:** Verify ads are blocked when adblocking is ON and Acceptable Ads are OFF.

    Given I have adblocking enabled and AA disabled on eyeo Chromium SDK .
    When I open "https://www.ask.com"
    And search for "laptops"
    Then I verify that ads are not displayed

**Scenario 2:** Verify ads are blocked when adblocking is ON and Acceptable Ads are ON.

    Given I have adblocking enabled and AA disabled on eyeo Chromium SDK
    When I open "https://www.ask.com"
    And search for "laptops"
    Then I verify that ads are displayed.

**Scenario 3:** Verify ads are blocked when adblocking is OFF.

    Given I have adblocking disabled on eyeo Chromium SDK
    When I open "https://www.ask.com"
    And search for "laptops"
    Then I verify that ads are displayed.

**Scenario 4:** Verify allowlisting feature is working properly.

    Given I have adblocking enabled
    When I add www.wikihow.com to allowlisted domains and 
    And open a subpage on www.wikihow.com
    Then I verify that ads are displayed


## Testing on ABP testpages
ABP testpages are designed to test the ad-filtering code against various types of html elements in a controlled environment.

You need to enable [More Blocking Options](#developer-options-for-testing) in order to run these tests.

### **Scenario 1** : Testing ABP testpages using custom filter lists

1. On the `More blocking options` screen, select the option of `Custom Filter lists`.
2. Enter the URL `https://abptestpages.org/en/abp-testcase-subscription.txt` in the text field and tap on the + icon.
3. Navigate to [Blocking - ABP Test Pages](https://abptestpages.org/en/filters/blocking).
4. Verify that you see green boxes, according to the descriptions of the test cases

<img src="images/abptestpages.jpg" alt="Screenshot: green boxes on the Blocking ABP Test Page" width="400"/>

### **Scenario 2** : Testing ABP testpages using custom filters

Alternatively, you can try adding custom filters rather than a custom filter list. This takes more effort but avoids unexpected interplay between filters defined for different test cases.

1. Navigate to [ABP Test Pages](https://abptestpages.org/) and click on the page you wish to test. For example: https://abptestpages.org/en/filters/blocking
2. Copy the filter mentioned on the testpage corresponding to the scenario under test.
3. On the `More blocking options` screen, select the option of `Custom Filters`.
4. Paste the filter to the custom filter field and refresh the page under test.
5. Verify the test state matches the test case description, typically by showing a green box.

## Filter lists

eyeo Chromium SDK needs filter lists in order to filter ads. Filter lists define which web resources should be blocked or hidden.

### Default filter lists

On first run, the browser will attempt to download and install the following default filter lists:
- [Easylist](https://easylist-downloads.adblockplus.org/easylist.txt), 
- [Acceptable Ads](https://easylist-downloads.adblockplus.org/exceptionrules.txt) (assuming Acceptable Ads is enabled by default),
- [Anti-Circumvention](https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt)
 
The SDK attempts to select a language-specific variant of Easylist, for example [Easylist+spanish](https://easylist-downloads.adblockplus.org/easylistspanish+easylist.txt) if the device's language is Spanish. Not all languages have such variants.

It will take a couple of seconds to download and install these lists. The user is free to browse the web while this is happening, and the SDK falls back to built-in, preloaded variants of the default lists to provide some level of ad-filtering.

### Downloading a filter list

The SDK downloads filter lists when:
- The browser starts for the first time and [the default lists are installed](#default-filter-lists)
- The user selects a new list in the [Language Filters menu](#settings-ui-android)
- The user adds a new custom filter list in the [More Blocking Options menu](#developer-options-for-testing)
- A filter list expires and [needs to be updated](#verifying-filter-list-update)

Every filter list download is a GET request to the URL of the filter list with a set of extra GET parameters.

### GET parameters

The SDK reports some information to eyeo via GET parameters added to the URL of each filter list.

This information is designed to preserve the user's anonymity and is used to:
- Count how many active users your browser attributes to the Acceptable Ads program
- Collect insights about what platforms eyeo should focus development efforts on

| Parameter                 | Value    |
| ----------------------- | ------- |
| addonName  | `eyeo-chromium-sdk` |
| addonVersion | `1.0` |
| application | The name of for your product. |
| applicationVersion | Follows Chromium's versioning by default, but can be overridden to reflect your product's version. |
| platform | `Windows`, `MacOSX`, `Linux` or `Android`, depending on the operating system. |
| platformVersion | `1.0` |
| lastVersion | Version of the filter list that is being updated, for example `202111101251`. Version is normally parsed from the filter list's "! Version: 202111101251" header comment. `0` if it's a new download or when a filter list doesn't declare a version. |
| disabled | `true` when Acceptable Ads is disabled in settings, `false` if it's enabled. |
| downloadCount | Total number of successful update downloads of the subscription. For anonymity reasons, clamped between `0` and `4+`. |

An example:
```
https://easylist-downloads.adblockplus.org/exceptionrules.txt?addonName=eyeo-chromium-sdk&addonVersion=1.0&application=Chromium&applicationVersion=110.0.5476.3&platform=Linux&platformVersion=1.0&lastVersion=202301021041&disabled=false&downloadCount=3
```

### Periodic pings of Acceptable Ads filter list

To not lose track of users that have disabled Acceptable Ads, the SDK performs periodic *pings* .

A *ping* is a HEAD-type filter list download request, very similar to ordinary filter list download requests. Because it's a HEAD (and not GET) request, the server will not respond with filter list content and the browser will not download Acceptable Ads. However the server will be made aware of the user's existence.

Pings are sent every 24 hours *while Acceptable Ads is disabled*. They are visible as HEAD requests to:
```
https://easylist-downloads.adblockplus.org/exceptionrules.txt?addonName=eyeo-chromium-sdk&...&disabled=true
```

### Periodic updates

The browser updates the filter lists according to their expiry times - typically every 24 hours. In order to check if filter lists are updated upon expiration, do the following:


1. Let all the default filter lists download successfully. 
2. On Android, force stop the application 
3. Navigate to phone settings to advance the time by 25 hours. 
4. Launch the application. 
5. Filter lists download requests should be sent to the server with an increased `downloadCount` in request parameter.
6. If you have [enabled VLOGs](#steps-to-enable-vlogdvlog-android), you should see relevant logs in the system console, for example:

```
... [eyeo] Running update check
... [eyeo] Updating expired subscription https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt
... [eyeo] Downloading https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt?addonName=eyeo-chromium-sdk&...&downloadCount=4+
... [eyeo] Finished downloading https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt, starting conversion
... [eyeo] Finished converting https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt successfully
... [eyeo] Updated subscription https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt, current version 202301121221
```

## Verifying Eyeometery

To learn about Eyeometry refer to: [user counting](../user-counting.md)

1. The browser sends an Eyeometry ping every 12 hours. You should see it in browser logs if you have [enabled VLOGs](#steps-to-enable-vlogdvlog-android)
```
... [eyeo] Telemetry request for https://eyeo-chromium.telemetry.eyeo.com/topic/eyeochromium_activeping/version/1 is due
... [eyeo] Telemetry request for https://eyeo-chromium.telemetry.eyeo.com/topic/eyeochromium_activeping/version/1 starting now
... [eyeo] Sending request to: https://eyeo-chromium.telemetry.eyeo.com/topic/eyeochromium_activeping/version/1
... [eyeo] Telemetry ping succeeded
```
2. If you see this line in the logs, your product wasn't built with a valid `client id`
```
[eyeo] Using default Telemetry server since a Telemetry client ID was not provided. Users will not be counted correctly by eyeo. Please set an ID via "eyeo_telemetry_client_id" gn argument.`
```

3. If you see this line in the logs, your product wasn't built with a valid `auth token`
```
[eyeo] No Telemetry authentication token defined. Users will not be counted correctly by eyeo. Please set a token via "eyeo_telemetry_activeping_auth_token" gn argument.
```


## Logging
eyeo Chromium SDK uses the following levels of logging:

1. `LOG(INFO/WARNING/ERROR)` - these logs appear in all builds. These are emitted relatively rarely, to avoid clutter.
2. `DLOG(INFO/WARNING/ERROR)` - these logs appear in debug build only
3. `VLOG(1/2/3/...)` - these logs appear in all builds if vmodule flag is set
4. `DVLOG(1/2/3/...)` - same as VLOG but only in debug builds

The browser writes these logs to the system console, also known as standard output. Do not confuse this with the Developer Tools console within the Inspector window.

See also [Chromium's instructions on logging](https://www.chromium.org/for-testers/enable-logging/)

### Steps to enable VLOG/DVLOG (Android):
1. Ensure `Enable command line on non-rooted devices` in chrome://flags is enabled
1. Run the following command:

`adb shell 'echo _ --enable-logging=stderr --vmodule="*subscription*=1,*activeping*=1,*adblock*=1,*converter*=1,*filtering_configuration*=1" > /data/local/tmp/chrome-command-line'`

Note: the flag persists until it is cleared

1. Launch the app now, manually, by tapping the icon on phone
1. Run `adb logcat` to view logs

Note: To check the currently set command line flags, run the command:

`adb shell 'cat /data/local/tmp/chrome-command-line'`

To clear the currently set flags:

`adb shell 'rm /data/local/tmp/chrome-command-line'`
