---
description: Customization options for the SDK.
---

# Settings

You can configure certain eyeo Chromium SDK settings to fit your use case.

The following table shows the settings you can change:

| Setting                 | Type    | Description                                                                                             |
| ----------------------- | ------- | ------------------------------------------------------------------------------------------------------- |
| Ad-blocking             | Boolean | Enables or disables ad-blocking                                                                         |
| Acceptable Ads          | Boolean | Displays or hides [Acceptable Ads](https://acceptableads.com/)                                          |
| Subscriptions           | List    | Filter list subscriptions                                                                               |
| Allowed domains         | List    | Domains on which no ads will be blocked, even if ad-blocking is enabled                                 |
| Custom filters          | List    | Additional filters written in [filter language](https://help.eyeo.com/adblockplus/how-to-write-filters) |

### Implementation details

You can configure settings with the C++ class `AdblockController` or its Java counterpart, `org.chromium.components.adblock.AdblockController`.

The Browser Extension API defined and documented in `chrome/common/extensions/api/adblock_private.idl` uses the C++ implementation. Android UI fragments consume the Java implementation.

The [user setting sequence diagram](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/raw/eyeo-110-dev/components/adblock/docs/settings/user-setting-sequence.png) describes the flow of updating a setting and how it varies depending on the API.
