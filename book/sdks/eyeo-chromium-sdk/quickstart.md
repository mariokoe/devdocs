---
description: Get up and running with the eyeo Chromium SDK.
---

# Quickstart

### About the eyeo Chromium SDK

eyeo Chromium SDK is a software development kit (**SDK**) that you can use to integrate eyeo's ad-filtering technology into Chromium-based browsers.

Maintained as a set of patches added to the [Chromium project](https://chromium.googlesource.com/chromium/src/), eyeo Chromium SDK lets you incorporate eyeo's ad-blocking functionality and filter lists into your own browser.

You can get started with the eyeo Chromium SDK in just two stages:

1. Verify your Chromium dependencies.
2. Fork the eyeo Chromium SDK from eyeo's GitLab repository.

By the end of this quickstart guide, you'll have cloned the eyeo Chromium SDK project and laid the ground work for the development of your own Chromium-based ad-filtering browser.

{% hint style="info" %}
This guide assumes familiarity with both Git and the command line.
{% endhint %}

### Verify Chromium dependencies

Because eyeo Chromium SDK is built as a Chromium fork, integrating eyeo Chromium SDK into your browser requires several Chromium dependencies.

A successful integration requires the following dependencies:

* [KeyedService](https://source.chromium.org/chromium/chromium/src/+/main:components/keyed_service/core/keyed_service.h)
* [Network Service](https://source.chromium.org/chromium/chromium/src/+/main:components/services/network)
* [PrefService](https://source.chromium.org/chromium/chromium/src/+/main:components/prefs/pref_service.h)
* [Profile](https://source.chromium.org/chromium/chromium/src/+/main:chrome/browser/profiles/profile.h)
* [Resources](https://source.chromium.org/chromium/chromium/src/+/main:components/resources)
* [Version Information](https://source.chromium.org/chromium/chromium/src/+/main:components/version_info)

If any of these dependencies is disabled in your configuration, the SDK might not perform as expected.

For questions about your implementation or advice on strategies that best fit your use case, reach out to your eyeo distribution partner.

### Clone the eyeo Chromium SDK fork

You'll build your eyeo Chromium SDK project on top of eyeo's current fork. Follow these steps to clone the eyeo Chromium SDK GitLab project:

1. **Copy the following URL:**

```html
https://gitlab.com/eyeo/adblockplus/chromium-sdk
```

1. **Clone the repository using the URL you just copied, then move in to the new eyeo Chromium SDK directory:**

```bash
git clone https://gitlab.com/eyeo/adblockplus/chromium-sdk
cd chromium-sdk
```

1. **Create your development branch from the latest eyeo Chromium SDK release tag**:

```bash
git checkout -b my-branch eyeo-abp-release-101.0.4951.41-2.0
```

At this point you can build a browser following [standard instructions](https://chromium.googlesource.com/chromium/src/+/main/docs/#Checking-Out-and-Building) and do commits to experiment with SDK features.

Now you have to apply your patch set from `origin` set above eyeo Chromium SDK. If you prefer to apply our code over yours, there is an option to get SDK as a patch. Please reach your conact in eyeo.

1. **Set eyeo Chromium SDK as the upstream repository, and set your repository as its origin:**

```bash
git remote rename origin upstream
git remote add origin your-repository-path
```

### Next up

In this quickstart guide, you cloned the eyeo Chromium SDK fork to your own development project.

While you could begin working on your own browser immediately, using the SDK to accomplish basic ad-filtering tasks will take your project to the next level.

Check out the [eyeo Chromium SDK Features documentation](sdk-features/) to learn how eyeo Chromium SDK handles common ad-filtering use cases.
