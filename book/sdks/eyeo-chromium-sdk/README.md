---
description: The eyeo Chromium SDK documentation home.
---

# eyeo Chromium SDK

The eyeo Chromium SDK is a software development kit (**SDK**) that you can use to integrate eyeo's ad-filtering technology into Chromium-based browsers.

Maintained as a set of patches added to [Chromium](https://chromium.googlesource.com/chromium/src/), the eyeo Chromium SDK enables seamless integration without requiring end users to install additional extensions or software.

The SDK provides a consistent experience across desktop and Android platforms by filtering out disruptive ads, pop-ups, video pre-rolls and pop-under tabs.

Because the eyeo Chromium SDK is fully integrated into a browser, Manifest V3 limitations have no effect on it.

You'll find the following documentation useful as you implement the eyeo Chromium SDK into your own projects:

* [Quickstart](quickstart.md); a walkthrough of a basic eyeo Chromium SDK installation
* [SDK Features](sdk-features/); an in-depth guide to customizing eyeo Chromium SDK
* [SDK Settings](sdk-features/settings.md); details on SDK settings you can change
* [ADRs & FAQs](adrs-and-faqs.md); the eyeo Chromium SDK architectural decision records and answers to frequently asked questions
* [User Counting](user-counting.md); information on how eyeo protects user privacy

### Next up

Take your first steps with the [eyeo Chromium SDK quickstart guide](quickstart.md).
