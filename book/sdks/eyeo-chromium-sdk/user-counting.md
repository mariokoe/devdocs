---
description: How eyeo protects privacy while counting users.
---

# User Counting

To improve eyeo's understanding of cross-platform usage and versioning, the eyeo Chromium SDK collects some user data.&#x20;

Because one of eyeo's driving principles is to respect user privacy, no requests sent to data collection services contain any personally identifiable information **(PII)**.

This page contains information on how and why eyeo implements user counting in eyeo Chromium.

### Why eyeo count users

eyeo count users who have both ad-blocking **and** [Acceptable Ads scheme](https://acceptableads.com/) enabled, as well as those who only have ad-blocking enabled. eyeo does this for the following reasons:

* Understanding user distribution across Chromium and SDK versions helps eyeo decide which SDK versions to maintain and which to deprecate.
* Understanding user distribution across operating systems and platforms (like mobile, desktop, gaming consoles, and so on) helps eyeo prioritize quality assurance and automation efforts on the most popular platforms and OS versions.
* Understanding how users perceive Acceptable Ads, which eyeo determines from user opt-out rate, contributes to the Acceptable Ads standard. eyeo wants to ensure that ads that users see don't negatively impact user experience.
* User counting is the basis of payouts to partners who monetize their Acceptable Ads users.

### How user counting works

A ping request is sent automatically by the SDK to eyeo's dedicated service at least twice per day, if the browser is in use (including in the background) and ad-blocking is enabled. These contain no personally identifiable information.

#### Set build arguments for user counting

With the eyeo Chromium SDK, you can count the number of users your product has. The eyeo Chromium SDK requires the following `gn` arguments to count active users:

* `abp_telemetry_client_id`
* `abp_telemetry_activeping_auth_token`

eyeo provides the argument values. To set the arguments, generate project build files like the following:

{% code overflow="wrap" %}
```gn
gn gen --args='abp_telemetry_client_id="mycompany" abp_telemetry_activeping_auth_token="peyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" ...' ...
```
{% endcode %}

If you don't set these arguments, eyeo may be not correctly attribute users to your product.

{% hint style="warning" %}
Keep your auth token secure. Never embed it in open-source repositories or documents. If your auth token is revealed, reach out to eyeo as soon as possible to receive a new token.
{% endhint %}

User counting doesn't transfer any PII or other identifiable data to eyeo, nor does it allow tracking or profiling of users by eyeo.

#### Ping request payload

To track active clients while maintaining user privacy, the server combines the values of ping timestamps with a randomly-generated tag. Dates are truncated to further protect privacy.

The following table outlines the parameters sent with the ping:

| Parameter             | Type   | Description                                                                                                                                                                                                                                                                                                |
| --------------------- | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `platform`            | string | Client operating system. Possible choices: `"Android"`, `"Windows NT"`, `"Mac OS X"`, `"Android_TV"`, `"iOS"`, `"Linux"`. Example: `"Linux"`                                                                                                                                                               |
| `platform_version`    | string | Version of the operating system. Example: `"10"`                                                                                                                                                                                                                                                           |
| `application`         | string | Browser name or product name (smart TV application, for example). It is recommended to use `version_info::GetProductName` for this value. Example: `"MyBrowser"`                                                                                                             i                             |
| `application_version` | string | Version  of the browser or product. Example: `"86.0.4240.183"`                                                                                                                                                                                                                                             |
| `addon_name`          | string | Name of the SDK integration. Example: `"eyeochromium"`                                                                                                                                                                                                                                                     |
| `addon_version`       | string | Version of the SDK. Example: `"4.24.0"`                                                                                                                                                                                                                                                                    |
| `first_ping`          | string | Datetime of the first successful eyeometry ping in timestamp format. Extracted from 2xx server response. Truncated to day. Example: `"2020-12-05T00:00:00.000Z"`                                                                                                                                           |
| `last_ping`           | string | Datetime of the last successful eyeometry ping, in timestamp format. Extracted from 2xx server response. Truncated to day. Example: `"2021-01-31T00:00:00.000Z"`                                                                                                                                           |
| `last_ping_tag`       | string | Random ID that is unique for each last_ping. This helps to anonymously distinguish between several ping requests with the same value of `last_ping`. Algorithm to be used to generate this is UUID4. If a ping is retried, the same value should be used. Example: `"be62e49f-d94e-411b-a060-1abc82d21b64"`|
| `previous_last_ping`  | string | Datetime of the previous to last eyeometry successful ping, in timestamp format.  Extracted from 2xx server response. Example: `"2021-01-29T00:00:00.000Z"`                                                                                                                                                |
| `aa_active`           | bool   | `true` if client opted in for Acceptable Ads, `false` otherwise                                                                                                                                                                                                                                            |
