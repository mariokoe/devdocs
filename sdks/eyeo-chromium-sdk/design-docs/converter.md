# Flatbuffer converter

The flatbuffer converter is a tool that converts an Adblock filter list into Flatbuffer binary format. 
This results in a more compact storage format that is faster to parse and uses less memory.

## Design overview

The Flatbuffer converter is stateless, with only one static function [`Parse`](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/tree/eyeo-111-dev/components/adblock/core/converter/converter.h) with a return value of the type `ConverterResult`. This can hold either of the following three values:

- the converted [Flatbuffer data](#flatbuffer-schema) on successful parsing,
- a redirect URL in case of a filter list with a redirect header,
- a `ConversionError` string in case of an error.

The converter pre-parses the filter list metadata to validate the type of the filter list (AdblockPlus filters are supported), and to check if the filter list redirects. This is done by the [Metadata](#metadata) parser.

The filters get parsed line-by-line. The converter classifies the filter and calls the proper [parser](#parser) for each filter. Upon successful parsing it gets [serialized](#serializer) into the appropriate table(s).

### Flatbuffer schema

The Flatbuffer schema defines the format the data will be serialized to. In eyeo Chromium SDK a filter list is represented as a [`subscription`](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/blob/eyeo-111-dev/components/adblock/core/schema/filter_list_schema.fbs).

### Metadata

The metadata parser parses the filter stream until the first non-comment line. It returns a Metadata object which contains the metadata for the filter list. (homepage, title, version, redirect URL (if any), expiration time (the default is 5 days)).

Since the parser has to peek the next line to see if it's a comment, it makes sure to rewind the stream to the end of the last comment line when finished. 

### Parser

There is a parser for all the three types of filters. These are stateless too and have a single static `Parse` function.

For more info see 

- [`ContentFilter`](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/tree/eyeo-111-dev/components/adblock/core/converter/parser/content_filter.h), 
- [`SnippetFilter`](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/tree/eyeo-111-dev/components/adblock/core/converter/parser/snippet_filter.h) and 
- [`UrlFilter`](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/tree/eyeo-111-dev/components/adblock/core/converter/parser/url_filter.h) classes.

### Serializer

The [`flatbuffer serializer`](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/tree/eyeo-111-dev/components/adblock/core/converter/serializer/flatbuffer_serializer.h) builds the `subscription` table from the parsed filter objects.

## Standalone converter

The standalone converter is used for converting pre-loaded filter lists during builds, and can be used to validate a filter list (see [Logging](#logging))

It is built with the `adblock_flatbuffer_converter` target and requires the following arguments to run: `adblock_flatbuffer_converter [INPUT_FILE] [FILTER_LIST_URL] [OUTPUT_FILE]` where 
- `[INPUT_FILE]`:  Path to a file with a list of [Adblock filters](https://eyeo.gitlab.io/adblockplus/abc/core-spec), each on a separate line
- `[FILTER_LIST_URL]`: The URL of the filter list
- `[OUTPUT_FILE]`: Path to the file where the flatbuffer data will be stored

## Testing

The flatbuffer converter is covered with [unit tests](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/tree/eyeo-111-dev/components/adblock/core/converter/test/flatbuffer_converter_test.cc). You can also find unit tests for just the parser [here](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/tree/eyeo-111-dev/components/adblock/core/converter/parser/test).

There are some [performance tests](https://gitlab.com/eyeo/adblockplus/chromium-sdk/-/tree/eyeo-111-dev/components/adblock/core/converter/test/flatbuffer_converter_perftest.cc) which measure the time it takes to convert `easylist.txt` and `exceptionrules.txt`.

## Logging

The converter logs detailed error messages if it encounters a filter that it was not able to process. In such cases the specific filter is skipped and the conversion continues.

To enable verbose logs and print them to standard error use the following run time arguments: `--enable-logging=stderr --v=1`

For more information about logging see [Chromium's logging page for testers](https://www.chromium.org/for-testers/enable-logging/).
